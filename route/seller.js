const express = require('express');
const sellerController = require('../controller/seller');
const countryStateCity = require('../controller/countryStateCity');
const verifyToken = require('../auth/verifyToken');
const router = express.Router();

router.post('/signUpSeller',sellerController.signUpSeller);
router.post('/sellerLogin',sellerController.sellerLogin);
router.post('/updateStore',verifyToken.verifyToken,sellerController.updateStore);
router.post('/updateUserProfile',verifyToken.verifyToken,sellerController.updateUserProfile);


// set master router.

// set list of country state and city.

router.get('/getAllCountry',verifyToken.verifyToken,countryStateCity.getCountry);
router.get('/getAllState/:country_id',verifyToken.verifyToken,countryStateCity.getAllState);
router.get('/getAllCity/:state_id',verifyToken.verifyToken,countryStateCity.getAllCity);


// check only token is valid or not.
router.get('/checkToken',verifyToken.verifyToken,verifyToken.isValid);
module.exports = router;
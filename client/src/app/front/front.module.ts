import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FrontRoutingModule } from './front-routing.module';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { FrontComponent } from './front.component';
import { PriceingComponent } from './priceing/priceing.component';
import { UserServicesService } from '../services/user-services.service';
import { SignupSetupComponent } from './seller-register/signup-setup/signup-setup.component';
import { TellUsPageComponent } from './seller-register/tell-us-page/tell-us-page.component';
import { UserprofileSetupComponent } from './seller-register/userprofile-setup/userprofile-setup.component';
import { SellerLoginComponent } from './seller-login/seller-login.component';


@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    FrontComponent,
    PriceingComponent,
    SignupSetupComponent,
    TellUsPageComponent,
    UserprofileSetupComponent,
    SellerLoginComponent
    ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FrontRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    
   
    
  ],
  providers: [UserServicesService],
  bootstrap: [FrontComponent]
})
export class FrontModule {
  
}

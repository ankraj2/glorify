import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FrontModule } from '../../front.module';
import { UserServicesService } from '../../../services/user-services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tell-us-page',
  templateUrl: './tell-us-page.component.html',
  styleUrls: ['./tell-us-page.component.css']
})
export class TellUsPageComponent implements OnInit {
  tellUsForm:FormGroup;

  // set display dropdown status.
  public display_option:any;

  // declare array of iteam.

  public selling_status_array:string[] = ["I'm selling, just no online","I sell with a different system","I'm just playing around","I'm no selling products yet","Trying it out"];

  public shopping_plateform_array:string[] = ["Other","Shopify","Revel","Opencart","Magento"];

  public creating_store_status_array:string[] = ['I want to start a new business',"I want to move my buginess to glorify"];

  public when_launch_store_array:string[] = ["I'm just playing around with Shopify","I'm learning and need more time","I'll be ready in a few weeks","I'm ready to go"];

  public how_to_sell_array:string[] = ["Online only","In person only","Both online and in person"];

  public selling_product_option_array: string[] =  ["Playing around with glorify.","brainstorming on what to sell","have a product and would be ready to sell in a few weeks"];

  public current_revenue_array: string[] = ["$0 (I'm just getting started)","Up to $5,000","$50,000 to $250,000","$1,000,000+"];


  constructor(private userService:UserServicesService,public router: Router) { }

  ngOnInit() {
      this.setDisplayFalse();
    this.tellUsForm = new FormGroup({
      selling_status: new FormControl('',Validators.required),
      shopping_plateform: new FormControl('',),
      creating_store_status: new FormControl('',),
      when_launch_store: new FormControl('',),
      how_to_sell: new FormControl('',),
      selling_product_option: new FormControl('',),
      current_revenue: new FormControl('',Validators.required)
    })
  }

  onSubmit(){
    this.userService.updateStore(this.tellUsForm.value).subscribe((response)=>{
     
      this.router.navigate(['seller/profile_setup']);

    },(error)=>{
      this.userService.errorHandle('somthing went wrong with server!','');
      
    })
  }

  sellingStatus(value){
    this.setDisplayFalse();
       if(value == 1){
        this.display_option.shopping_plateform = true;
       }
       if(value == 2){
            this.display_option.when_launch_store = true;
            this.display_option.how_to_sell = true;
       }
       if(value == 3){
        this.display_option.selling_product_option = true;
        
       }
      
  }

  shoppingPlateform(value){
    
    this.display_option.creating_store_status = true;
  }

  setDisplayFalse(){
    this.display_option = {'selling_status':true,'shopping_plateform':false,'creating_store_status':false,'when_launch_store':false,'how_to_sell':false,'selling_product_option':false,'current_revenue':true};
  }
}

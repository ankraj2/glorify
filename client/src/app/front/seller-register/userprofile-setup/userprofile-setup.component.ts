import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MinLengthValidator } from '@angular/forms';
import { validateConfig } from '@angular/router/src/config';
import { UserServicesService } from '../../../services/user-services.service';
import { Router } from '@angular/router';
import { CountryStateCityComponent } from '../../../common_component/country-state-city/country-state-city.component';

@Component({
  selector: 'app-userprofile-setup',
  templateUrl: './userprofile-setup.component.html',
  styleUrls: ['./userprofile-setup.component.css']
})
export class UserprofileSetupComponent implements OnInit {

  userProfileForm:FormGroup;
  allCountry:any;
  allState:any;
  allCity:any;
  constructor(public userService: UserServicesService,public router: Router) { }

  ngOnInit() {

    this.getCountry();



    this.userProfileForm = new FormGroup({
      first_name : new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required),
      street_address: new FormControl('',Validators.required),
      address_line_one: new FormControl('',Validators.required),
      city_name: new FormControl('',Validators.required),
      zip_code: new FormControl('',Validators.compose([Validators.required,Validators.minLength(6),Validators.maxLength(6)])),
      country_id: new FormControl('',Validators.required),
      state_id: new FormControl('',Validators.required),
      mobile_no: new FormControl('',Validators.required),
      website: new FormControl('',)

    })
  }

  ngSubmit(){
    this.userService.updateUserProfile(this.userProfileForm.value).subscribe((response)=>{
      this.userService.openSnackBar('congratulations, Your seller account has been created!','');
      this.router.navigate(['/admin']);
     
    },
    (error)=>{
      this.userService.errorHandle('somthing went wrong with server!','');
  
      
    });
  }

  getCountry(){
    this.userService.getAllCountry().subscribe((response)=>{
    
      this.allCountry = response['data'];
    },(error)=>{
      this.userService.errorHandle('somthing went wrong with server!','');
      
    })
  }

  getAllState(country_id){
    this.userService.getAllState(country_id).subscribe((response)=>{
      console.log(response);
      this.allState = response['data'];
    },(error)=>{
      this.userService.errorHandle('somthing went wrong with server!','');
    })
  }

  getAllCity(state_id){
    this.userService.getAllCity(state_id).subscribe((response)=>{
      this.allCity = response['data'];
    },(error)=>{
      this.userService.errorHandle('somthing went wrong with server!','');
    })
  }

  back(){
    this.router.navigate(['/seller/account_setup']);
  }
}

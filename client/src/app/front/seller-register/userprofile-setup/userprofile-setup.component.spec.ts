import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserprofileSetupComponent } from './userprofile-setup.component';

describe('UserprofileSetupComponent', () => {
  let component: UserprofileSetupComponent;
  let fixture: ComponentFixture<UserprofileSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserprofileSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserprofileSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserServicesService } from '../../services/user-services.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.component.html',
  styleUrls: ['./seller-register.component.css']
})
export class SellerRegisterComponent implements OnInit {
  
  SellerRegistFrom: FormGroup;
  isUnique: String;
  finalResponce:any;
  constructor(public router:Router,public userServeces:UserServicesService) { }

  ngOnInit() {
    this.SellerRegistFrom = new FormGroup({
      email: new FormControl('',Validators.compose([Validators.required,Validators.email])),
      password: new FormControl('',Validators.compose([Validators.required,Validators.min(5)])),
      store_name: new FormControl('',Validators.required)
    });
  }

  onSubmit(){
    // call services for calling server api.
    this.userServeces.sellerRegister(this.SellerRegistFrom.value).subscribe(
      (data) => {
        this.finalResponce =  data;
        
        //store data in local storage like token.
        localStorage.setItem(
          'userData',
          JSON.stringify({'token':this.finalResponce.data.token })
        );  
          
          
        // redirect account/setup route.    
        this.router.navigate(['/account/setup']);
        
       }, // success path

      (error) => {
        // error handle here.
        if(error.status == 422){
            if(error.error.data.errors['0'].validatorKey == 'isUnique'){
              this.isUnique = "This Email already used";
            }else{
              this.userServeces.errorHandle('Server side validation issue','');
            }
        }else{
          this.userServeces.errorHandle('somthing went wrong with server!','');
        }
       }
    );
  }

}

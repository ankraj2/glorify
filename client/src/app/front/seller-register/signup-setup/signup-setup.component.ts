import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signup-setup',
  templateUrl: './signup-setup.component.html',
  styleUrls: ['./signup-setup.component.css']
})
export class SignupSetupComponent implements OnInit {

  constructor(public router:Router) { }
  text:string[] = ['Sit tight, we are creating your store...','Sucess Your store is ready to go'];
  inner_text:string[] = ['1 of 3: Creating your account','2 of 3: Setting your profile','3 of 3: Applying store setting'];
  fronImageUrl:string = environment.fronImageUrl;
  number:number;
  displayText:string;
  innerDisplayText:string;
  innerNumber:number;
  startTime = new Date().getTime();
  processid:any;
  ngOnInit() {


    this.number = 0;
    this.innerNumber = 0;
    
    this.displayText = this.text['0'];
    this.innerDisplayText = this.inner_text['0'];

   
    // call interval for inner text to be display.
    setInterval(()=>{
      this.innerDisplayText = this.inner_text[this.innerNumber];
      this.innerNumber = this.innerNumber+1
    },1000);


    // call interval for outer text.
    this.processid = setInterval(()=>{

      // return after time completed.
      if(new Date().getTime() - this.startTime > 9000){
        // clear process of interval.
        this.router.navigate(['seller/account_setup']);
        clearInterval(this.processid);
        return;
      }
      
      // set text for frontent.
      this.displayText = this.text[this.number];
      this.number = this.number+1
    },3000);
  }

}

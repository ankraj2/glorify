import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupSetupComponent } from './signup-setup.component';

describe('SignupSetupComponent', () => {
  let component: SignupSetupComponent;
  let fixture: ComponentFixture<SignupSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

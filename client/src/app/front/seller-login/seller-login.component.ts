import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserServicesService } from '../../services/user-services.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-seller-login',
  templateUrl: './seller-login.component.html',
  styleUrls: ['../seller-register/seller-register.component.css']
})
export class SellerLoginComponent implements OnInit {

  SellerLoginFrom: FormGroup;
  isUnique: String;
  finalResponce:any;

  constructor(public router:Router,public userServeces:UserServicesService) { }

  ngOnInit() {
    this.SellerLoginFrom = new FormGroup({
      email: new FormControl('',Validators.compose([Validators.required,Validators.email])),
      password: new FormControl('',Validators.compose([Validators.required,Validators.min(5)])),
      store_name: new FormControl('',Validators.required)
    });
  }


  onSubmit(){
    // call services for calling server api.
    this.userServeces.sellerlogin(this.SellerLoginFrom.value).subscribe(
      (data) => {
        this.finalResponce =  data;
        
        //store data in local storage like token.
        localStorage.setItem(
          'userData',
          JSON.stringify({'token':this.finalResponce.data.token })
        );  
          
        // redirect account/setup route.    
        this.router.navigate(['/admin']);
       }, // success path
      (error) => {
        // error handle here.
        if(error.status == 401){
          this.userServeces.errorHandle('User email and password is wrong','');
        }else{
          this.userServeces.errorHandle('somthing went wrong with server!','');
        }
       }
    );
  }


}

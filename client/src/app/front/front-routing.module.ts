import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FrontComponent } from './front.component';
import { PriceingComponent } from './priceing/priceing.component';
import { TellUsPageComponent } from './seller-register/tell-us-page/tell-us-page.component';
import { SignupSetupComponent } from './seller-register/signup-setup/signup-setup.component';
import { SellerLoginComponent } from './seller-login/seller-login.component';

import { UserprofileSetupComponent } from './seller-register/userprofile-setup/userprofile-setup.component';

import { AuthGuard } from '../services/auth-guard.service';


const frontRoutes: Routes = [
    { path: '',
        redirectTo: '/',
        pathMatch: 'full'
    },

    {   
        path: '', component: FrontComponent,
        children: [
            {
                path: '', component : HomeComponent,
            },
            {
                path: 'pricing', component: PriceingComponent
            },
        ]
    },
    {
        path:'account/setup',canActivate:[AuthGuard],component:SignupSetupComponent
    },
    {
        path:'seller/account_setup',canActivate:[AuthGuard],component: TellUsPageComponent
    },
    {
        path:'seller/profile_setup',canActivate:[AuthGuard],component: UserprofileSetupComponent
    },
    {
        path:'seller/login',component: SellerLoginComponent 
    }

  ];

@NgModule({
   
    imports:[
        RouterModule.forChild(frontRoutes)
    ], 
    exports: [ RouterModule ],
})  
export class FrontRoutingModule{
    
}
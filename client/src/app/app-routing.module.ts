import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component'; 
import { PageNotFountdComponent } from './page-not-fountd/page-not-fountd.component';
import { AdminComponent } from './admin/admin.component';
import { SellerRegisterComponent } from './front/seller-register/seller-register.component';

const appRoutes: Routes = [
   { path: 'register', component: SellerRegisterComponent}
   
  ];

@NgModule({
   
    imports:[
        RouterModule.forRoot(appRoutes,)
    ], 
    exports: [ RouterModule ],
})  
export class AppRoutingModule{
    
}
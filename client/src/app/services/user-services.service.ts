import { Injectable } from '@angular/core';
  import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class UserServicesService {

  constructor(private http:HttpClient,public snackBar: MatSnackBar) { }

  // this function help to get token
  getToken(){
    return JSON.parse(localStorage.getItem('userData')).token;
  }

  getHeader(){
    return new HttpHeaders({'Content-Type':'application/json','x-access-token':this.getToken()});
  }
  
  json(response){
    return response.json().data;
  }

  /**
   * Name: sellerRegister
   */

  sellerRegister(fomrData){
    return this.http.post(environment.apiUrl+'seller/signUpSeller',fomrData);
  }

  sellerlogin(fomrData){
    return this.http.post(environment.apiUrl+'seller/sellerLogin',fomrData);
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,      
    });
  }

  /**
   * Name: errorHandle
   * Description: This is responsible for displaying server error 
   * @param message 
   * @param error 
   */
  errorHandle(message,error){
    if(error.status == 403){
      this.openSnackBar('Token is missing for this user','Please login');
      return false;
    }
    this.openSnackBar(message,'');
  }

  /**
   * Name: updateStore
   * Desc: This function help to getting token data.
   * @param fomrData 
   */
  updateStore(formData){
    return this.http.post(environment.apiUrl+'seller/updateStore',formData,{headers:this.getHeader()
    });
  }

  updateUserProfile(formData){
  
    return this.http.post(environment.apiUrl+'seller/updateUserProfile',formData,{headers:this.getHeader()
    });
  }

  getAllCountry(){
    return this.http.get(environment.apiUrl+'seller/getAllCountry',{
      headers:this.getHeader()
    });
  }

  getAllState(country_id){
    return this.http.get(environment.apiUrl+'seller/getAllState/'+country_id,{
      headers:this.getHeader()
    });
  }

  getAllCity(country_id){
    return this.http.get(environment.apiUrl+'seller/getAllCity/'+country_id,{
      headers:this.getHeader()
    });
  }

  isValid(token){
    console.log(this.getHeader());
    return this.http.get(environment.apiUrl+'seller/checkToken/',{
      headers:this.getHeader()
    });
  }
  
}

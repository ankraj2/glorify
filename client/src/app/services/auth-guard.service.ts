import { CanActivate, 
        ActivatedRoute,
        ActivatedRouteSnapshot,
        RouterStateSnapshot,
        Router,
        CanActivateChild,
        CanDeactivate
        }
    from '@angular/router';
import { Injectable }     from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { UserServicesService } from './user-services.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild{

    constructor(private authService:AuthService,private router: Router,public userService:UserServicesService){}

    result:boolean = false;
    canActivate(route: ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean
    {   
        
        return this.authService.isAuthenticated()
        .then(
            (authentication: boolean)=>{
                // if not false.
                
                if(authentication != false){

                    // call api for checking token.
                    this.userService.isValid(authentication).subscribe((response)=>{
                        this.result = true;
                    },(error)=>{
                        this.router.navigate(['/']);
                    }) 
                }else{
                    this.router.navigate(['/'])
                }
                return true;
            }
        )
    }
    canActivateChild(route: ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean
    {
        return this.canActivate(route,state);
    }
    
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { FrontModule } from './front/front.module';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFountdComponent } from './page-not-fountd/page-not-fountd.component';
import { SellerRegisterComponent } from './front/seller-register/seller-register.component';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { UserServicesService } from './services/user-services.service';
import { MatSnackBarModule } from '@angular/material';
import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';



@NgModule({
  declarations: [
    AppComponent,
    PageNotFountdComponent,
    SellerRegisterComponent,
    ],
  imports: [
    BrowserModule,
    AdminModule,
    AppRoutingModule,
    FrontModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatSnackBarModule,
    
  ],
  providers: [UserServicesService,AuthGuard,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
  
}

import {
  Compiler, Component, Injector, VERSION, ViewChild, NgModule, NgModuleRef,
  ViewContainerRef,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-products',
  // templateUrl: './products.component.html',
  // styleUrls: ['./products.component.css']
  template: `
      <ng-container #vc></ng-container>
  `
})

export class ProductsComponent implements OnInit {

  @ViewChild('vc', {read: ViewContainerRef}) vc;
  name = `Angular! v${VERSION.full}`;
  public data:string;

  constructor(private _compiler: Compiler,
              private _injector: Injector,
              private _m: NgModuleRef<any>) { }
  
    ngAfterViewInit() {
      const tmpCmp = Component({
          moduleId: module.id, templateUrl: './products.component_t.html'})(class {
            

      });
      
      const tmpModule = NgModule({declarations: [tmpCmp]})(class {
      });
  
      this._compiler.compileModuleAndAllComponentsAsync(tmpModule)
        .then((factories) => {
          const f = factories.componentFactories[0];
          const cmpRef = f.create(this._injector, [], null, this._m);
          cmpRef.instance.name = 'dynamic';
          this.vc.insert(cmpRef.hostView);
        })
    }


  ngOnInit() {
    //this.templateUrl = './products.component.html';
    this.data = "Ankit tiwari";
  }

}

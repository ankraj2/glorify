import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from "@angular/core";
import { RouterModule, Router, Routes } from "@angular/router";
import { AdminComponent } from './admin.component';

import { ProductsComponent } from './products/products.component';
import { CustomersComponent } from './customers/customers.component';
import { OrdersComponent } from './orders/orders.component';
import { AddProductComponent } from './products/add-product/add-product.component';

const adminRouter:Routes = [
    {   
        path: 'admin', component: AdminComponent,
        children: [
            {
              path: 'product', component : ProductsComponent,
            },
            {
                path: 'add-product', component : AddProductComponent,
              },
            {
                path: 'customer', component: CustomersComponent
            },
            {
                path: 'order', component: OrdersComponent
            }
    ]
    },
   
]
@NgModule({
    imports:[
        RouterModule.forChild(adminRouter)
    ],
    exports:[
        RouterModule
    ]
})
export class AdminRoutingModule{

}
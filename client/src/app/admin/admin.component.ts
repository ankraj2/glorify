import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
})
export class AdminComponent implements OnInit {
  public adminRoute:boolean = false;
  constructor(private route: ActivatedRoute,public router: Router) {
 
   }

  ngOnInit() {
    if(this.router.url == '/admin'){
      this.adminRoute = true;
    }else{
      this.adminRoute = false;
    }
    console.log(this.adminRoute);
    
  }

}

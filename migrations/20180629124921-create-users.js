'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name:{
        type:Sequelize.STRING(80)
      },
      last_name:{
        type: Sequelize.STRING(80)
      },
      street_address:{
        type: Sequelize.TEXT
      },
      address_line_one:{
        type:Sequelize.TEXT
      },
      city_id:{
        type:Sequelize.INTEGER
      },
      zip_code:{
        type:Sequelize.STRING(6)
      },
      country_id:{
        type: Sequelize.INTEGER
      },
      state_id:{
        type:Sequelize.INTEGER
      },
      mobile_no:{
        type:Sequelize.STRING(11)
      },
      email:{
        type:Sequelize.STRING(100)
      },
      password:{
        type:Sequelize.STRING
      },
      website:{
        type:Sequelize.STRING(50)
      },
      role_id:{
        type: Sequelize.INTEGER(2)
      },
      status:{
        type:Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      deleted:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};
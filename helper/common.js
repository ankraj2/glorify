const sequelize = require('sequelize');
const models = require('../models/index');
module.exports = {
    sendJson:sendJson,
    commonLog:commonLog,
    getStoreInfo:getStoreInfo

}
/**
 * Name:sendJson
 * Description: this is create json response and send to client.
 * Auther:Ankit Tiwari
 */
function sendJson(res,statusCode,statusFlage,data,message,error = false){
    return res.status(statusCode)
        .json({
            data: data,
            error:error,
            message:message,
            statusCode: statusFlage
            
        });
}

function sendErrorJson(res,error){
    return res.status(500)
        .json({
            status: false,
            data: error,
            message:'something went to wrong on your server'
        });
}
/**
 * Name: commonLog
 * Auther: Ankit Tiwari
 * Description: This function help to maintain activity log.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function commonLog(req,request_type,permission_type,json_response,status,next){
    let data = {};
    
    // set all value in object. 
    data.fk_se_emp_id = req.emp_id;
    data.request_type = request_type;
    data.permission_type = permission_type;
    data.json_response = json_response;  
    data.ip_address = req.connection.remoteAddress;
    data.browser = req.headers['user-agent'];
    data.status = status; 
    // make object for build data.
    const ml_log = models.ml_log.build(data);    
    // save data in ml_log table.
    ml_log.save();
}

async function getStoreInfo(userId){
    return new Promise(async function(resolve,reject){
        try{
            let record = await models.stores.findAll({
                where:{
                    user_id:userId
                }
            })
    
            if(record.length > 0){
                // return sucess 
                resolve({id:record['0'].id,store_name:record['0'].store_name});

            }else{
                // no record found.
                resolve('null');
            }
        }catch(error){
           // console.log(error);
            reject(error);
        }
       
       
    })
}

const sequlize = require('sequelize');
const models = require('../models/index');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const config  = require('../config/');
const helper = require('../helper/common');
const constants = config.constants;
const md5 = require('md5');
module.exports = {
    signUpSeller:signUpSeller,
    sellerLogin:sellerLogin,
    updateStore:updateStore,
    updateUserProfile:updateUserProfile
}
/**
 * name: signUpSeller
 * Desc: This funciton help to create seller.
 * Auther: Ankit Tiwari.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function signUpSeller(req,res,next){

    // define a function for insert seller in db.
    try{
        req.body.role_id = config.constants.USER_TYPE.seller.id;
        // build all request data with field
        const user = models.users.build(req.body);
        
        // user data save into database. 
        let result = await user.save();
        // if data saved in users tabel.
        if(!result.id){
            throw "data not saved in users table";
        }
        // build query with value with model field.
        let store_info = {store_name:req.body.store_name,user_id:result.id}
            
        // save data in stores table.
        let store = await models.stores.build(store_info);
        let store_result = store.save(store_info);
        // call seller login api.
        sellerLogin(req,res,next);
       // let success_result = {user_id:result.id,store_id:store_result.id}

        //return helper.sendJson(res,config.constants.SUCCESS,true,success_result,'registration sucesfully',false);
        
    }catch(error){
        
        let err_code = config.constants.SERVER_ERROR;
        let error_msg = 'server error';
        if(typeof error.errors['0'].type != 'undefined' &&  error.errors['0'].type == 'Validation error')
        {
            err_code =config.constants.VALIDATION_ERROR;
            error_msg = 'Validation error';
        }
        return helper.sendJson(res,err_code,false,error,error_msg,true)
    }
    
}

/**
 * Name:sellerLogin
 * Desc: This function help to login and create token for each seller.
 * Author: Anskit Tiwari
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function sellerLogin(req,res,next){
    let respon_data = {};
    try{
        let result = await models.users.findAll({
            where:{
                email:req.body.email,
                password:md5(req.body.password),
                role_id:2
            }
        });
        // no record found.
        if(!result.length){
            respon_data.error_code = constants.AUTH_FAIL;
            respon_data.error_msg = " User email and password is wrong";
            throw respon_data;
        }
        
        // if user exist then create token.
        const token = jwt.sign({id:result[0].id},config.config.secret,{expiresIn: 84400})
      
       
        return helper.sendJson(res,config.constants.SUCCESS,true,{'token':token},'login sucesfully',false);
        
    }catch(error){
        return helper.sendJson(res,error.error_code,false,error,error.error_msg,true)
        
    }
}

/**
 * Name: UpdateStore
 * Desc: This funciton help to updating only store info.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function updateStore(req,res,next){
    let response_data = {};
    try{
        response_data.error_code = constants.SERVER_ERROR;
        response_data.error_msg = 'Server wrong your server';
        // get store info using user id.
        const storeInfo = await helper.getStoreInfo(req.userId);
       
        if(storeInfo == 'null'){
            response_data.error_code = constants.NOT_ACCEPTABLE;
            response_data.error_msg = 'store not found of this user';
            throw response_data
        }
        // update store data using store id.
        let result = await models.stores.update(req.body,{where:{id:storeInfo.id}});

        // return response to client.
        return helper.sendJson(res,config.constants.SUCCESS,true,result,'update store sucessfully',false);

    }catch(error){
     
        // return error response to client.
        return helper.sendJson(res,error.error_code,false,error.error_msg,'record could not be update',true)
       
    }
}

/**
 * Name: updateUserProfile
 * Desc: This funciton help to update all user profile.
 * Author: Ankit Tiwari
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function updateUserProfile(req,res,next){
    console.log(req.body);
    let response_data = {};
    try{
        

        // update store data using store id.
        let result = await models.users.update(req.body,{where:{id:req.userId}});

        // return response to client.
        return helper.sendJson(res,config.constants.SUCCESS,true,result,'update user sucessfully',false);

    }catch(error){
        console.log(error);
        // return error response to client.
        return helper.sendJson(res,constants.SERVER_ERROR,false,error,'record could not be update',true)
       
    }
}

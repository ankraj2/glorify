const sequlize = require('sequelize');
const models = require('../models/index');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const config  = require('../config/');
const helper = require('../helper/common');
const constants = config.constants;
module.exports = {
    getCountry:getCountry,
    getAllState:getAllState,
    getAllCity:getAllCity
}

/**
 * Name: getCountry
 * Description: This function get all list of country.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function getCountry(req,res,next){

    try{
        let result = await models.countries.findAll({
            attributes:['id','name']
        });

        return helper.sendJson(res,config.constants.SUCCESS,true,result,'Get all countries',false);

        
    }catch(error){
        return helper.sendJson(res,constants.SERVER_ERROR,false,error,'record could not be find',true)
        
    }
}

/**
 * Name: getAllState
 * Description: This function get all list of state.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function getAllState(req,res,next){

    try{
        let result = await models.states.findAll({
            where:{
                country_id:req.params.country_id
            },
            attributes:['id','name']
        });

        return helper.sendJson(res,config.constants.SUCCESS,true,result,'Get all states',false);

        
    }catch(error){
        return helper.sendJson(res,constants.SERVER_ERROR,false,error,'record could not be find',true)
        
    }
}

/**
 * Name: getAllCity
 * Description: This function get all list of city.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function getAllCity(req,res,next){

    try{
        let result = await models.cities.findAll({
            where:{
                state_id:req.params.state_id
            },
            attributes:['id','name']
        });

        return helper.sendJson(res,config.constants.SUCCESS,true,result,'Get all city',false);

        
    }catch(error){
        return helper.sendJson(res,constants.SERVER_ERROR,false,error,'record could not be find',true)
        
    }
}
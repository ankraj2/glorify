'use strict';
const md5 = require('md5');
module.exports = (sequelize, DataTypes) => {
  var states = sequelize.define('states', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name:{
      type:DataTypes.STRING(80),
      allowNull: true,
    },
    
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  states.associate = function(models) {
    // associations can be defined here
  };
 return states;
};
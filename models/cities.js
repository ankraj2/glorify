'use strict';
const md5 = require('md5');
module.exports = (sequelize, DataTypes) => {
  var cities = sequelize.define('cities', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name:{
      type:DataTypes.STRING(80),
      allowNull: true,
      
    },
    state_id:{
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  cities.associate = function(models) {
    // associations can be defined here
  };

  return cities;
};
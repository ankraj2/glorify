'use strict';
const md5 = require('md5');
module.exports = (sequelize, DataTypes) => {
  var users = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name:{
      type:DataTypes.STRING(80),
      allowNull: true,
      validate:{
        is:{args:["^[a-zA-Z]+$",'i'],msg:"first name should be only alphabet"},
   
      }
    },
    last_name:{
      type: DataTypes.STRING(80),
      allowNull: true,
      validate:{
        is:{args:["^[a-zA-Z]+$",'i'],msg:"last name should be only alphabet"},
       
      }
    },
    street_address:{
      type: DataTypes.TEXT,
      allowNull: true,
      validate:{
        notEmpty:{ args:[true],msg:"street_address should not empty string"},  
      }
    },
    address_line_one:{
      type:DataTypes.TEXT,
      allowNull: true,
      validate:{
        notEmpty:{ args:[true],msg:"street_address should not empty string"},  
      },
     
    },
    city_name:{
      type:DataTypes.STRING(50),
      allowNull: true,
    },
    zip_code:{
      type:DataTypes.STRING(6),
      validate:{
        is:{args:["^[0-9]+$",'i'],msg:"zip_code should be only number"},
       
      },
      allowNull: true,
    },
    country_id:{
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    state_id:{
      type:DataTypes.INTEGER,
      allowNull: true,
    },
    mobile_no:{
      type:DataTypes.STRING(11),
      validate:{
        is:{args:["^[0-9]+$",'i'],msg:"mobile_no should be only number"},
      }
    },
    email:{
      type:DataTypes.STRING(100),
      allowNull: false,
      validate:{
        isEmail:{ args:[true],msg:"emial formate wrong" },
        isUnique: function (email, done) {
          users.find({ where: { email: email}}).done(function(err,user){
            if (err) {
              done('code already registered');
            }
            done();
          })
        }
      }
    },
    password:{
      type:DataTypes.STRING,
      allowNull: false,
      validate:{
        notEmpty:{ args:[true],msg:"password should not empty string"},  
      }
    },
    website:{
      type:DataTypes.STRING(50),
      allowNull: true,
    },
    role_id:{
      type: DataTypes.INTEGER(2),
      validate:{
        is:{args:["^[0-9]+$",'i'],msg:"role_id should be only number"},
        
      }
    },
    status:{
      type:DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted:{
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  users.associate = function(models) {
    // associations can be defined here
  };

  // declare hook.
  users.beforeCreate((user,options)=>{
    user.password  = md5(user.password)
  })
  return users;
};
'use strict';
const md5 = require('md5');
module.exports = (sequelize, DataTypes) => {
  var countries = sequelize.define('countries', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    sortname:{
      type:DataTypes.STRING(3),
      allowNull: true,
     
    },
    name:{
      type: DataTypes.STRING(150),
      allowNull: true,
      
    },
    phonecode:{
      type: DataTypes.INTEGER,
      allowNull: true,
      
    },
    
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  countries.associate = function(models) {
    // associations can be defined here
  };

 
  return countries;
};
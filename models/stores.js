'use strict';
module.exports = (sequelize, DataTypes) => {
  var Stores = sequelize.define('stores', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id:{
      type:DataTypes.INTEGER,
      allowNull: false,
    },
    store_name:{
        type:DataTypes.STRING,
        allowNull: false,
    },
    selling_status:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    shopping_plateform:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    creating_store_status:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    when_launch_store:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    how_to_sell:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    selling_product_option:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    current_revenue:{
      type:DataTypes.STRING(2),
      allowNull: true,
    },
    status:{
      type:DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    deleted:{
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {});
  Stores.associate = function(models) {
    // associations can be defined here
  };
  return Stores;
};
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const commonHelper = require('../helper/common');
const config = require('../config/config');


module.exports = {
    sellerSignUp: sellerSignUp
}

function sellerSignUp(req,res,next){
    return new Promise(function(resolve,reject){
        req.checkBody('email','Email is missing').notEmpty();
        req.checkBody('password','Password is missing').notEmpty();
        req.checkBody('store_name','Store name is missing').notEmpty();
        
        // check validation.
        req.getValidationResult().then(function(result){
            if(!result.isEmpty()){
                commonHelper.sendJson(res,401,401,result.array(),'Validation Error',true);
            }
            resolve(true);
        }).catch(function(error){
            commonHelper.sendJson(res,500,500,error,'Validation Error',true);
        });
            
       
    })
}

var express = require('express');
var path = require('path');
var cors = require('cors');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var md5 = require('md5');
var sha1 = require('sha1');
const verifyToken = require('./auth/verifyToken');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var config = require(__dirname+'/config/config');

var seller = require(__dirname+'/route/seller');

var app  = express();

// set for cros origin
app.use(cors());

// set bodyparser middleware.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// set server side validation middlevare.
app.use(expressValidator());

app.use(express.static(__dirname + '/client/dist/'));

app.use('/seller',seller);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/client/dist/index.html'));
});


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
 });  

function next(err){
    console.log("err",err);
}
app.listen(4201,function(){
    console.log("Server is started! on port 4201.");
});